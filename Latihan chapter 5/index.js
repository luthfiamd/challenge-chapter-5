const express = require('express');
const app = express();
const loginPlayer = require('./router/player')

app.use('/public', express.static('public'));
app.use(express.json())
app.set('view engine','ejs')

var login = function (req, res, next) {
    res.render('login.ejs')
    next()
  }
  
app.get('/home', (req, res) => {
    res.render('pagehome')
});

app.get('/login', (req, res) => {
    res.render('login')
  });
  

app.get('/views/game.ejs', (req, res) => {
    res.render('game')
});

app.listen(3000, () => {
    console.log(`Server started on port`);
});