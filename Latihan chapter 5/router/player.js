const express = require("express");
const router = express.Router()


let player = [
    {
        id : 1,
        name : "Dadang",
        username : "Admin1",
        pass : "dadangaja"
    },
    {
        id : 2,
        name : "Asep",
        username : "Admin2",
        pass : "asepaja"
    }
]

router.post('/api/backend/login', (req, res) => {
    const {name, username, pass} = req.body

    if (name === undefined || username === undefined || pass === undefined){
        res.status(400).json("Bad request")
        return
    }

    const nextId = player.length + 1

    player.push({
        id : id,
        name : name,
        username : username,
        pass : pass
    })

    res.status(200).json("Username and Password correct!")
})

router.get('/api/backend/login/', (req, res) => {
    res.status(200).json(player)
})

router.get('/api/backend/login/:id', (req, res) => {
    const id = req.params.id
    const filterAdmin = login.find(i => i.id == id)

    if (filterAdmin === undefined){
        res.status(400).json("Data User is not found!")
        return
    }
    
    res.status(200).json(filterAdmin)

})

router.put('/api/backend/login/:id', (req, res) => {
    const id = req.params.id
    const {username, pass, name} = req.body

    if (username === undefined || pass === undefined || name === undefined){
        res.status(400).json("Bad request")
    }

    let found = false
    for (let i = 0; i<login.length; i++){
        if (login[i].id == id){
            login[i].username = username,
            login[i].pass = pass,
            login[i].nama = name
            found = true
            break
        }
    }

    if (found == true){
        res.status(200).json("Updated!")
        return
    } else {
        res.status(400).json("Bad request!")
        return
    }
})

router.delete('/api/backend/login/:idUser', (req, res) => {
    const id = req.params.id
    const searchIdUser = login.find(i => i.id == id)

    if (searchIdUser === undefined){
        res.status(400).json("Data User is not found!")
        return
    }

    const index = login.indexOf(searchIdUser)

    login.splice(index, 1)
    res.status(200).json("Deleted!")
})

module.exports = router
